using System;
using System.Drawing;
using GXPEngine;

public class MyGame : Game //MyGame is a Game
{	
	//initialize game here
	public MyGame () : base(1366, 1024, false)
	{
		Background background = new Background();
		AddChild (background);

        Menu menu = new Menu();
        AddChild(menu);

	    
	}
	
	//update game here
	void Update ()
	{
		//empty
	}
	
	//system starts here
	static void Main() 
	{
		new MyGame().Start();
	}

 /*   public void AddEndScreen()
    {
        game.Add(EndMenu as _endscreen);

    }
    */
}
