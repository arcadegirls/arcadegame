﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GXPEngine
{
    public class Level : GameObject
    {
        private Player _player;
        private Boss _boss;
        private Obstacle _obstacle;
      //  private bool _gameIsRunning;
       

        List<List<int>> levelList = new List<List<int>>();
        List<ChaseEnemy> chaseEnemyList = new List<ChaseEnemy>();
        


        public Level(int levelNumber) : base()
        {
            //_gameIsRunning = true;
            
            switch (levelNumber)
            {
                case 1:
                    addLevel1();
                    break;
                case 2:
                    //addLevel2();
                    break;
            } 

            _player = new Player(this);
            AddChild(_player);
            _player.x = levelList[0][0];
            _player.y = levelList[0][1];

            _boss = new Boss();
            AddChild(_boss);
            _boss.x = levelList[6][0];
            _boss.y = levelList[6][1];

            // _player.onGameEnd = onGameEnd;




            for (int i = 0; i < levelList[1].Count; i++)
            {
                _obstacle = new Obstacle();
                AddChild(_obstacle);
                _obstacle.x = levelList[1][i];
                _obstacle.y = levelList[1][i+1];
                i++;
            }

            for (int i = 0; i < levelList[2].Count; i++)
            {
                Enemy enemy = new Enemy(levelList[2][i], levelList[2][i +1], levelList[2][i +2], levelList[2][i +3]);
                AddChild(enemy);
                i += 3;
             
            }
           
            Lion lion = new Lion();
            AddChild(lion);
            lion.x = levelList[3][0];
            lion.y = levelList[3][1];

            chaseEnemyList.Clear();
            for (int i = 0; i < levelList[4].Count; i++)
            {
                ChaseEnemy chaseEnemy = new ChaseEnemy(levelList[4][i], levelList[4][i + 1], levelList[4][i + 2], levelList[4][i + 3], _player);
                AddChild(chaseEnemy);
                chaseEnemyList.Add(chaseEnemy);
                i += 3;
                //  enemy.SetTarget(_player);
            }

            for (int i = 0; i < levelList[5].Count; i++)
            {
                HidingSpot _hidingspot = new HidingSpot();
                AddChild(_hidingspot);
                _hidingspot.x = levelList[5][i];
                _hidingspot.y = levelList[5][i + 1];
                i++;
            }

        }

        private void addLevel1()
        {
            levelList = new List<List<int>>();
            List<int> playerInfo = new List<int>();
            playerInfo.Add(50); // player.x  levelList[0][0]
            playerInfo.Add(1000); // player.y levelList[0][1]
            levelList.Add(playerInfo);

            List<int> obstacleInfo = new List<int>();
          //  obstacleInfo.Add(300);// obstacle1.x
          //  obstacleInfo.Add(900);// obstacle1.y
            // making the floor 
            obstacleInfo.Add(0); // obstacle2.x
            obstacleInfo.Add(1010); // obstacle2.y
            obstacleInfo.Add(100); // obstacle3.x
            obstacleInfo.Add(1010); // obstacle3.y
            obstacleInfo.Add(200); // obstacle4.x
            obstacleInfo.Add(1010); // obstacle4.y
            obstacleInfo.Add(300); // obstacle5.x
            obstacleInfo.Add(1010); // obstacle5.y
            obstacleInfo.Add(500); // obstacle6.x
            obstacleInfo.Add(1010); // obstacle6.y
            obstacleInfo.Add(600); // obstacle7.x
            obstacleInfo.Add(1010); // obstacle7.y
            obstacleInfo.Add(700); // obstacle8.x
            obstacleInfo.Add(1010); // obstacle8.y
            obstacleInfo.Add(800); // obstacle9.x
            obstacleInfo.Add(1010); // obstacle9.y
            obstacleInfo.Add(900); // obstacle10.x
            obstacleInfo.Add(1010); // obstacle10.y
            obstacleInfo.Add(1000); // obstacle11.x
            obstacleInfo.Add(1010); // obstacle11.y
            obstacleInfo.Add(1100); // obstacle12.x
            obstacleInfo.Add(1010); // obstacle12.y
            obstacleInfo.Add(1200); // obstacle13.x
            obstacleInfo.Add(1010); // obstacle13.y
            obstacleInfo.Add(1300); // obstacle14.x
            obstacleInfo.Add(1010); // obstacle14.y
            obstacleInfo.Add(1400); // obstacle15.x
            obstacleInfo.Add(1010); // obstacle15.y
            obstacleInfo.Add(1500); // obstacle16.x
            obstacleInfo.Add(1010); // obstacle16.y
            levelList.Add(obstacleInfo);

            List<int> enemyInfoList = new List<int>();
            enemyInfoList.Add(1); //enemy1.x
            enemyInfoList.Add(0); //enemy1.y
            enemyInfoList.Add(0); //enemy1.direction
            enemyInfoList.Add(4); //enemy1.length
            

            enemyInfoList.Add(3); //enemy2.x
            enemyInfoList.Add(1); //enemy2.y
            enemyInfoList.Add(1); //enemy2.direction
            enemyInfoList.Add(3); //enemy2.length

            enemyInfoList.Add(6); //enemy3.x
            enemyInfoList.Add(1); //enemy3.y
            enemyInfoList.Add(0); //enemy3.direction
            enemyInfoList.Add(3); //enemy3.length
            levelList.Add(enemyInfoList);

            List<int> lionInfoList = new List<int>();
            lionInfoList.Add(1020); //Lion.x
            lionInfoList.Add(800); //Lion.y
            levelList.Add(lionInfoList); 

            List<int> chaseEnemyList = new List<int>();
            chaseEnemyList.Add(1); //enemy1.x
            chaseEnemyList.Add(0); //enemy1.y
            chaseEnemyList.Add(0); //enemy1.direction
            chaseEnemyList.Add(4); //enemy1.length
            levelList.Add(chaseEnemyList);

            List<int> hideSpotsList = new List<int>();
            hideSpotsList.Add(0); // obstacle2.x
            hideSpotsList.Add(890); // obstacle2.y
            hideSpotsList.Add(600); // obstacle2.x
            hideSpotsList.Add(900); // obstacle2.y
            levelList.Add(hideSpotsList);

            List<int> bossInfoList = new List<int>();
            bossInfoList.Add(900);  //boss.x
            bossInfoList.Add(800);  //boss.x
            levelList.Add(bossInfoList);



        }

        private void addLevel2()
        {
            levelList.Clear();




        }

        public void EnemyChase(bool mayEnemyChase)
        {
            foreach (ChaseEnemy enemy in chaseEnemyList)
            {
                enemy.canChase = mayEnemyChase;
            }
        }
        
  /*      public void onGameEnd()
        {
            //destroy all child object automatically
            while (GetChildren().Count > 0)         //while we have child object
            {
                GetChildren()[0].Destroy();         //remove first child object
            }

            _player = null;                         //when the game ends player doesn't exist
            _gameIsRunning = false;

        }

       public void AddEndScreen()
        {
            game.Add(EndMenu as Endscreen);

        }
        */
    }
}
