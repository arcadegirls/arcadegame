﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GXPEngine
{
    class HighscoreTimeManager : GameObject
    {
        private float playerPlayTime = 0.0f;
        private bool startTimer = false;

        public void StartTimer()
        {
            startTimer = true;
        }

        public void pauseTimer(bool stopTheTimer)
        {
            if (stopTheTimer) startTimer = false;
            else startTimer = true;
        }

        public float GetTime()
        {
            return playerPlayTime;
        }

        public bool GetTimerState()
        {
            return startTimer;
        }

        void Update()
        {
            if (!startTimer) return;
            playerPlayTime += Time.deltaTime;
        }
    }
}
