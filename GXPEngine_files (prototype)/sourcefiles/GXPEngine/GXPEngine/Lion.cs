﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GXPEngine
{
    public class Lion : Sprite
    {
        Sound coinSound;

        public Lion() : base("media/lion_prototype.png")
        {
            coinSound = new Sound("media/coin2.wav");

        }

        public void Pickup()
        {
            coinSound.Play();
            Destroy();
           // coinSound.Play();
        }

    }
}
