﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GXPEngine
{
    public class Enemy : Sprite
    {
        GameObject target;

        int UNIT = 136;
        int speed = 5;
        int time = 0;
        int length = 0;
        int direction = 0;

        public Enemy(int x, int y, int direction, int length) : base("media/skull_color.png")
        {

            this.x = x * UNIT;
            this.y = y * UNIT + 500;
            this.length = length * UNIT / 3;
            this.time = 0;
            this.direction = direction;

            target = null;
        }

        public void SetTarget(GameObject target)
        {
            this.target = target;
        }

        void Update()
        {
            if (this.direction == 0)
            {
                x = x + speed;        //speed of the enemy
            }
            else
            {
                y = y + speed;
            }

            time = time + 1;
            if (time == this.length)        //when enemy reaches end of screen - return
            {
                speed = -5;
            }
            if (time >= this.length * 2)
            {
                speed = 5;
                time = 0;
            }

            if (target != null)     //if there is a target,
            {
                if(x > target.x)    //move towards it
                {
                    Move(2, 0);
                }
                if (x > target.x)
                {
                    Move(-2, 0);
                }
                if (y > target.y)
                {
                    Move(0, 2);
                }
                if (y > target.y)
                {
                    Move(0, -2);
                }
            }

        }
    }
}
