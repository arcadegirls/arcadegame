﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GXPEngine
{
    class ChaseEnemy : Sprite
    {
        GameObject target;

        int UNIT = 136;
        int speed = 2;
        int time = 0;
        int length = 0;
        int direction = 0;
        public bool canChase = true;
        Player player;

        public ChaseEnemy(int x, int y, int direction, int length, Player player) : base("media/skull_color.png")
        {
            this.x = x * UNIT;
            this.y = y * UNIT + 500;
            this.length = length * UNIT / 3;
            this.time = 0;
            this.direction = direction;
            this.player = player;

            target = null;
        }

        public void SetTarget(GameObject target)
        {
            this.target = target;
        }

        void Update()
        {
            moveTowardsPlayer(player.x, player.y);
        }

        public void moveTowardsPlayer(float playerX, float playerY)
        {
            if (canChase)
            {
                if (x > playerX)
                {
                    tryMove( -speed, 0);
                }else if (x < playerX)
                {
                    tryMove(speed,0);
                }
                if (y > playerY)
                {
                    tryMove(0, -speed);
                }
                else if (y < playerY)
                {
                    tryMove(0, speed);
                }
            }
            else
            {

            }
        }

        void tryMove(float moveX, float moveY)
        {
            x = x + moveX;
            y = y + moveY;
            foreach (GameObject other in GetCollisions())
            {
                if (other is Obstacle)
                {
                    x = x - moveX;
                    y = y - moveY;
                    return;

                }
            }
        }
    }
}
