﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GXPEngine
{
    public class Timer : GameObject
    {
        private int _time;
        private Action _onTimeOut;

        public Timer(int timeOut, Action onTimeOut) : base()
        {
            _onTimeOut = onTimeOut;
            _time = timeOut;

        }

        private void Update()
        {
            _time -= Time.deltaTime;
            if(_time <-0)
            {
                
                _onTimeOut();
                Destroy();
            }

        }

    }
}
