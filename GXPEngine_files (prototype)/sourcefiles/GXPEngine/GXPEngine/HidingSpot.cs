﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;

namespace GXPEngine
{
    public class HidingSpot : AnimationSprite
    {
        int step;
        const int ANIMATION_FRAMEWAIT = 80;

        public HidingSpot() : base("media/Rock_animation.png", 2, 1)
        {
            step = 0;

        }

        private void Update()
        {
            step = step + 1;
            if (step > ANIMATION_FRAMEWAIT)
            {
                NextFrame();
                step = 0;
            }

        }
    }
}
