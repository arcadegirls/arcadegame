﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GXPEngine
{
   public class Menu : GameObject
    {
        Button _button;
        MenuScreen _menuscreen;
        bool _hasStarted;
        HighscoreTimeManager _highscoretimer;
        ScoreCanvas _scorecanvas;
       // xmlreader _xmlreader;

        public Menu() : base()
        {
            _menuscreen = new MenuScreen();
            AddChild(_menuscreen);

            _hasStarted = false;
            _highscoretimer = new HighscoreTimeManager();
            AddChild(_highscoretimer);
            _scorecanvas = new ScoreCanvas(300,20);
            AddChild(_scorecanvas);
        //    _xmlreader = new xmlreader(500,50);
        //    AddChild(_xmlreader);

            //create button in the middle of the screen
            _button = new Button();
            AddChild(_button);
            _button.x = (game.width - _button.width) / 2;
            _button.y = (game.height - _button.height) / 2;

        }

        void Update()
        {
            //if the button is pressed the game starts
            if (Input.GetMouseButtonDown(0))
            {
                if (_button.HitTestPoint(Input.mouseX, Input.mouseY))
                {
                    startGame();
                }
            }
            if (_highscoretimer.GetTimerState())
            {
                float currentTime = _highscoretimer.GetTime();
                currentTime /= 1000;

                string curentscore = string.Format("Current Time: {0}", currentTime.ToString());
                
                _scorecanvas.SetScore(curentscore);
            }
        }

        void hideMenu()
        {
            _button.visible = false;
            _menuscreen.visible = false;
        }

        void startGame()
        {
            if (_hasStarted == false)
            {
                Level level = new Level(1);
                AddChild(level);
                _hasStarted = true;
                hideMenu();

                Sound music = new Sound("media/ambience-cave-00.mp3", true, true);
                music.Play();
                _highscoretimer.StartTimer();


            }

        }

    }
}
