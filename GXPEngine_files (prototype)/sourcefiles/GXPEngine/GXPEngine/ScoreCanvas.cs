﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace GXPEngine
{
    class ScoreCanvas : Canvas
    {
        public ScoreCanvas(int width, int height) : base(width, height)
        {

        }

        public void SetScore(string curentScore)
        {
            graphics.Clear(Color.Black);
            graphics.DrawString(curentScore, SystemFonts.DefaultFont, Brushes.White, 0, 0);
            
        }
    }
}
