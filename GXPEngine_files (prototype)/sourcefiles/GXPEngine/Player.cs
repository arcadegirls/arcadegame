using System;

namespace GXPEngine
{
    public class Player : AnimationSprite
    {
        int speed = 3;
        int step;
        const int ANIMATION_FRAMEWAIT = 8;
        float checkpointX;
        float checkpointY;
        //  int lives = 3;
        //  public static int NUMLIVES;
     //   SoundChannel musicChannel;
        //Sound heartSound;

        /// <summary>
        /// Jumping vars
        /// </summary>
    /*    bool canJump = true;
        int JUMPFORCE = 200;
        float gravity = 0.5f;
        float gravityIncreased = 0.0f;  */
        Level level;
        bool startHidingspotHitTest = false;
        GameObject currenthidingSpot;

      //  public Action onGameEnd;


        public Player(Level lvl) : base("media/spritesheetmary.png", 4, 2)
        {
            SetOrigin(width / 2, height / 2);
            level = lvl;

            this.scale = 0.7f;

            //add graphical layer on top of player
            /*    Sprite gfx = new Sprite("media/mary.png");
                gfx.SetOrigin(gfx.width / 2, gfx.height);//set origin in bottom-center
                AddChild(gfx);  */

            step = 0;

            checkpointX = 0.0f;         //checkpoint is at the bottom middle of the screen
            checkpointY = 0.0f;

          // Sound heartSound = new Sound("media/heart.mp3", true, false);
          // musicChannel = heartSound.Play();

        }

        public void SaveCheckPoint()
        {
            checkpointX = x;
            checkpointY = y;

        }

        private void Respawn()
        {

            x = checkpointX;
            y = checkpointY;

        }

        void Update()
        {
            if (Input.GetKey(Key.A))
            {
                tryMove(-speed, 0);
             //   x -= 4;
              
            }
            if (Input.GetKey(Key.D))
            {
               //  x += 4;
               tryMove(speed, 0);
                step = step + 1;
                if (step > ANIMATION_FRAMEWAIT)
                {
                    NextFrame();
                    step = 0;
                }
            }
            if (Input.GetKey(Key.W))
            {
                /* if (canJump == true)
                 {
                     // y -= 4;
                     tryMove(0, -JUMPFORCE);
                     canJump = false;
                 } */

                tryMove(0, -speed);
            }
            if (Input.GetKey(Key.S))
            {
                // y += 4;
                tryMove(0, speed);
            }
            
              /*  tryMove(0, gravity + gravityIncreased);
                gravityIncreased += 0.1f; */
               
            if(startHidingspotHitTest && !this.HitTest(currenthidingSpot))
            {
                startHidingspotHitTest = false;
                level.EnemyChase(true);
            }
        
              //limit movement, so girl can't leave screen here! (or add scrolling here)
              game.x = 100 - x;

        /*    if (Input.GetKey(Key.SPACE))
            {
                musicChannel.Stop();

            }  */
        }
         

        void OnCollision(GameObject other)
        {
            //all collision go here
            
        /*    if (other is Lion)
            {
                Lion lion = other as Lion;
                lion.Pickup();
            } */
            if (other is Enemy)
            {
               // Respawn();
            }
            if(other is HidingSpot)
            {
                level.EnemyChase(false);
                startHidingspotHitTest = true;
                currenthidingSpot = other;

                // musicChannel = heartSound.Play();
            }
            if(other is Boss)
            {
                Boss boss = other as Boss;
                boss.Merge();

               // EndMenu endmenu = new EndMenu();
               // AddChild(endmenu);
            }

        }

        void tryMove(float moveX, float moveY)
        {
            x = x + moveX;
            y = y + moveY;
            foreach (GameObject other in GetCollisions())
            {
                if (other is Obstacle)
                {
                    x = x - moveX;
                    y = y - moveY;
                  //  canJump = true;
                  //  gravityIncreased = 0.0f;
                    return;

                }
            }
        }

        

       public void GetLives()
        {


        }
    }

}
